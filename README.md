### Installation

```sh
 $ npm install
 $ cp .env.example .env
```
### Start dev server

```sh
  $ npm start
```
