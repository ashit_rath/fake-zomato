import React from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import LocationOn from 'material-ui/svg-icons/communication/location-on';
import GoogleMapReact from 'google-map-react';
import { OverlayTrigger, Popover } from 'react-bootstrap';

const tooltip = (id, name, address) => (
  <Popover id={'popover-' + id} title={name}>
    <p>{address}</p>
  </Popover>
)

const scrollTo = (id) => {
  document.getElementById(id).scrollIntoView()
  window.scrollBy(0, -80);
}

const Pin = ({ color, id, name, address }) => (
  <OverlayTrigger placement="bottom" overlay={tooltip(id, name, address)} >
    <IconButton onClick={() => scrollTo(id)} >
      <LocationOn color={color} />
    </IconButton>
  </OverlayTrigger>
)

const renderPins = (data) => {
  return data.restaurants.map(res => {
    const { location: { latitude, longitude, address }, name, id } = res.restaurant

    return (
      <Pin
        key={id}
        lat={latitude}
        lng={longitude}
        color="black"
        name={name}
        id={id}
        address={address}
      >
      </Pin>
    );
  });
}

const GMap = (props) => {
  const { location: { latitude, longitude }, data } = props

  return (
    <div className="map-main-wrapper">
      <GoogleMapReact
        bootstrapURLKeys={{
          key: "AIzaSyA8u45sHDsAZMahkKQUHEI1mpgMaaAe7XU",
          language: 'en'
        }}
        center={{ lat: latitude, lng: longitude }}
        defaultZoom={12}
      >
        {renderPins(data)}
        <Pin
          lat={latitude}
          lng={longitude}
          color="red"
          name="You!"
        />
      </GoogleMapReact>
    </div>
  )
}

GMap.propTypes = {
  location: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired
}

export default GMap;
