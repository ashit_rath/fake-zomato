import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Search from 'components/Search';

class Header extends Component {
  render() {
    const { onSearchSubmit, defaultSearchTerm } = this.props

    return (
      <header>
        <div className="container clearfix">
          <h3>Lookin' for Zomato ?</h3>
          <div className="header-search">
            <Search
              onSubmit={onSearchSubmit}
              defaultSearchTerm={defaultSearchTerm}
            />
          </div>
        </div>
      </header>
    )
  }
}

Header.propTypes = {
  onSearchSubmit: PropTypes.func.isRequired,
  defaultSearchTerm: PropTypes.string.isRequired
}

export default Header;
