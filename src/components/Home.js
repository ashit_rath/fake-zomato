import React, { Component } from 'react';
import Search from 'components/Search';
import { browserHistory } from'react-router';

class Home extends Component {
  onSearch = (searchTerm) => {
    browserHistory.push(`/search?searchTerm=${searchTerm}`);
  }

  render() {
    return (
      <div className="home">
        <div className="center-box">
          <h2>Lookin' for Zomato ?</h2>
          <Search onSubmit={this.onSearch} />
        </div>
      </div>
    )
  }
}

export default Home;
