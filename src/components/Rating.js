import React, { Component } from 'react';
import { Button, Popover, OverlayTrigger } from 'react-bootstrap';
import CircularProgress from 'material-ui/CircularProgress';
import { List } from 'material-ui/List';
import PropTypes from 'prop-types';

import Review from 'components/Review';
import { zomatoApi } from 'helpers/api';

class Rating extends Component {
  constructor() {
    super()

    this.state = {
      review: {},
      fetching: false,
      fetched: false
    }
  }

  popover = () => {
    const { review, fetching } = this.state

    if (this.state.fetching)
      return (
        <Popover id="popover-positioned-left" title="Reviews">
          <CircularProgress />
        </Popover>
      );

    if (!this.state.fetched)
      return (<div></div>);

    return (
      <Popover id="popover-positioned-left" title="Reviews">
        <List>
          {review.user_reviews.map(data => <Review key={data.review.id} data={data.review}/>)}
        </List>
      </Popover>
    );
  }

  onRatingMouseOver = () => {
    const { fetched, fetching } = this.state

    if (!fetched && fetching)
      return;

    if (!fetched) {
      this.setState({ fetching: true });

      zomatoApi.fetchReviews(this.props.data.id)
        .then(response => {
          this.setState({ review: response.data, fetching: false, fetched: true })
        })
    }
  }

  render() {
    const {
      user_rating: {
        aggregate_rating,
        votes,
        rating_color
      },
      id,
      name
    } = this.props.data

    return (
      <OverlayTrigger placement="left" overlay={this.popover()}>
        <div className="user-rating" onMouseOver={this.onRatingMouseOver}>
          <div className="rating" style={{ backgroundColor: `#${rating_color}` }}>{aggregate_rating}</div>
          <div className="voting">{votes} votes</div>
        </div>
      </OverlayTrigger>
    )
  }
}

Rating.propTypes = {
  data: PropTypes.object.isRequired
}

export default Rating;
