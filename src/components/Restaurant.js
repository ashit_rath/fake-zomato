import React from 'react';
import PropTypes from 'prop-types';

import Rating from 'components/Rating';
import RestaurantOptions from 'components/RestaurantOptions';

const Restaurant = ({ data }) => {
  let {
    id,
    name,
    average_cost_for_two,
    currency,
    cuisines,
    user_rating,
    menu_url,
    location,
    featured_image,
    thumb
  } = data,
  coverClass = '',
  coverClassStyle = {};

  thumb = thumb.length === 0 ? 'https://b.zmtcdn.com/images/placeholder_200.png?output-format=webp' : thumb;

  return (
    <section id={id} className="restaurant-card clearfix">
      <div className="content-box clearfix">
      <div className="thumb-img" style={{ backgroundImage: `url(${thumb})` }} />
      <Rating data={data} />
        <aside>
          <h5 className="name">{name}</h5>
          <p className="cuisines truncate">{cuisines}</p>
          <p className="address">{location.locality}</p>
          <p className="pricing">Cost for two - {currency}{average_cost_for_two}</p>
        </aside>
      </div>
      <RestaurantOptions restaurant={data}/>
    </section>
  )
}

Restaurant.propTypes = {
  data: PropTypes.object.isRequired
}

export default Restaurant;
