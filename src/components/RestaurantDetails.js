import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Restaurant from 'components/Restaurant';

class RestaurantDetails extends Component {
  renderRestaurant = (restaurants) => {
    return restaurants.map(res => {
      const { restaurant } = res

      return (
        <div className="col-lg-6" key={restaurant.id}>
          <Restaurant data={restaurant} />
        </div>
      )
    })
  }

  render() {
    const { data, error, searching } = this.props;

    return (
      <div className="main-restaurant-wrapper">
        <div className="row">
          {this.renderRestaurant(data.restaurants)}
        </div>
      </div>
    )
  }
}

RestaurantDetails.propTypes = {
  data: PropTypes.object.isRequired
}

export default RestaurantDetails
