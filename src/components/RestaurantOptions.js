import React from 'react';
import FA from 'react-fontawesome';
import PropTypes from 'prop-types';

const onlineDelivery = (restaurant) => {
  const { order_url, has_online_delivery } = restaurant

  if (has_online_delivery === 1) {
    return (
      <a href={order_url} className="action-item online-delivery" target="_blank"><FA name="images"/><FA name="shopping-cart" /> Order Online</a>
    )
  }

  return null
}

const tableBooking = (restaurant) => {
  const { has_table_booking, book_url } = restaurant

  if (has_table_booking === 1) {
    return (
      <a href={book_url} className=" action-item book-table" target="_blank"><FA name="calendar" /> Book Table</a>
    )
  }

  return null
}

const RestaurantOptions = ({ restaurant }) => {
  const { menu_url, photos_url } = restaurant

  return (
    <div className="action-items">
      <a href={menu_url} className="action-item" target="_blank"><FA name="bars" /> Menu</a>
      <a href={photos_url} className="action-item" target="_blank"><FA name="image" /> Photos</a>
      {onlineDelivery(restaurant)}
      {tableBooking(restaurant)}
    </div>
  );
}

RestaurantOptions.propTypes = {
  data: PropTypes.object
}

export default RestaurantOptions
