import React from 'react';
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import { darkBlack } from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider';
import PropTypes from 'prop-types';

const Review = ({ data }) => {
  const {
    review_text,
    review_time_friendly,
    rating,
    rating_color,
    profile_image,
    user: {
      name
    }
  } = data

  return (
    <ListItem
      primaryText={
        <p>{name}&nbsp;&nbsp;<span style={{color: `#${rating_color}`}}>{rating}</span></p>
      }
      secondaryText={
        <p>
          <span style={{color: darkBlack}}>{review_time_friendly}</span>
          &nbsp;{review_text}
        </p>
      }
      secondaryTextLines={2}
    />
  )
}

Review.propTypes = {
  data: PropTypes.object.isRequired
}

export default Review;
