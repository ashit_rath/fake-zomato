import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FA from 'react-fontawesome';

class Search extends Component {
  onKeyDown = (event) => {
    if (event.keyCode === 13) // On enter key press
      this.props.onSubmit(event.currentTarget.value)
  }

  render() {
    return (
      <div className="search-wrapper">
        <FA name="search"/>
        <input
          type="search"
          onKeyDown={this.onKeyDown}
          defaultValue={this.props.defaultSearchTerm}
          placeholder="Search Location"
        />
      </div>
    )
  }
}

Search.defaultProps = {
  defaultSearchTerm: ''
}

Search.propsTypes = {
  defaultSearchTerm: PropTypes.string,
  onSubmit: PropTypes.func.isRequired
}

export default Search
