import React from 'react'
import CircularProgress from 'material-ui/CircularProgress';
import PropTypes from 'prop-types';
import GMap from 'components/GMap';

import RestaurantDetails from 'components/RestaurantDetails';

const Message = ({ message }) => (
  <div className="search-result-wrapper">
    {message}
  </div>
)

const SearchResult = (props) => {
  const {
    searching,
    error,
    data,
    location
  } = props

  if (searching)
    return <Message message={<CircularProgress size={80} thickness={5} className="horizontal-center"/>} />
  if (error)
    return <Message message={<h3 className="text-center">Oops! Something went wrong.</h3>} />

  if (!data.restaurants.length)
    return <Message message={<h3 className="text-center">Enter location to start searching ...</h3>} />

  return (
    <div className="container search-result-wrapper">
      <GMap data={data} location={location} />
      <RestaurantDetails data={data} />
    </div>
  )
}

SearchResult.propTypes = {
  location: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
  error: PropTypes.bool.isRequired,
  searching: PropTypes.bool.isRequired
};

export default SearchResult;
