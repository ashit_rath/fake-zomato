import React, { Component } from 'react';
import Header from 'components/Header';
import PropTypes from 'prop-types';

import SearchResult from 'components/SearchResult';
import { zomatoApi } from 'helpers/api';

class Index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentLocation: {},
      searchData: { restaurants: [] },
      searching: false,
      error: false
    }
  };

  componentDidMount() {
    this.initiateSearch(this.props.location.query.searchTerm || '');
  }

  resetSearchFlags = () => {
    this.setState({ searching: true, error: false });
  }

  initiateSearch = (searchTerm) => {
    if (searchTerm.length === 0)
      return;

    this.resetSearchFlags()

    zomatoApi.searchLocation(searchTerm)
      .then(response => {
        const locationDetails = response.data.location_suggestions[0];

        this.setState({ currentLocation: locationDetails });

        return locationDetails;
      })
      .then(location => {
        return zomatoApi.searchRestaurant(location.entity_id, location.entity_type);
      })
      .then(response => {
        this.setState({ searchData: response.data, searching: false });
      })
      .catch(error => {
        this.setState({ serching: false, error: true });
      })
  }

  onSearchSubmit = (searchTerm) => {
    const newSearchTerm = searchTerm.trim();

    this.setState({ searchTerm: newSearchTerm });

    if (this.state.searchTerm !== newSearchTerm)
      this.initiateSearch(newSearchTerm);
  }

  render() {
    const {
      currentLocation,
      searchData,
      searching,
      error
    } = this.state;

    return (
      <div className="main">
        <Header
          onSearchSubmit={this.onSearchSubmit}
          defaultSearchTerm={this.props.location.query.searchTerm || '' }
        />
        <SearchResult
          searching={searching}
          error={error}
          data={searchData}
          location={currentLocation}
        />
      </div>
    )
  }
}

export default Index;
