import axios from 'axios';

const zomatoApi = (function() {
  const apiKey = '675a148578493293be45289af16ec51d',
        baseUri = 'https://developers.zomato.com/api/v2.1',
        request = axios.create({
          baseURL: baseUri,
          headers: {
            'user-key': apiKey
          }
        });

  const searchLocation = (location) => {
    return request.get('/locations', {
      params: {
        query: location
      }
    })
  };

  const searchRestaurant = (entityId, entityType) => {
    return request.get('/search', {
      params: {
        entity_id: entityId,
        entity_type: entityType
      }
    })
  };

  const fetchReviews = (resId) => {
    return request.get('/reviews', {
      params: {
        res_id: resId
      }
    })
  };

  return {
    searchLocation,
    searchRestaurant,
    fetchReviews
  }
})();

export {
  zomatoApi
};
