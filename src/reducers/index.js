import { combineReducers } from 'redux';

import sample from 'reducers/sample';

const appData = combineReducers({
  sample
})

const rootReducer = combineReducers({
  appData
});

export default rootReducer;
