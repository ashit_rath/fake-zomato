import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from 'components/app';
import Index from 'components/index';
import Home from 'components/Home';

const My404Component = () => <h1 className="text-center">404 !</h1>

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="search" component={Index} />
    <Route path='*' exact={true} component={My404Component} />
  </Route>
);
