const dotenv             = require('dotenv')
const path               = require('path')
const webpack            = require('webpack')
const merge              = require('webpack-merge')
const CompressionPlugin  = require('compression-webpack-plugin')
const DotenvPlugin       = require('webpack-dotenv-plugin')
const ExtractTextPlugin  = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin  = require('html-webpack-plugin')

const PATHS = {
  env: (process.env.HEROKU && './.env.heroku') || (process.env.NODE_ENV && `./.env.${process.env.NODE_ENV}`) || './.env',
  js: './index.js',
  css: './assets/stylesheets/main.scss'
}

// Load environment variables to be used in webpack.config.js
dotenv.config({ path: PATHS.env })

const config = {}

config.common = {
  context: path.join(__dirname, 'src'),
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: process.env.PUBLIC_PATH
  },
  module: {
    rules: [
      // Preloaders
      {
        test: /\.scss/,
        enforce: 'pre',
        loader: 'import-glob-loader'
      },

      // Loaders
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true
          }
        },
        exclude: /(node_modules|bower_components)/
      }, {
        test: /\.(jpe?g|png|gif|svg)$/,
        use: 'file-loader?name=images/[name]-[hash:8].[ext]'
      }, {
        test: /\.json$/,
        use: 'json-loader'
      }, {
        test: /\.(woff|woff2|ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'file-loader?name=fonts/[name]-[hash:8].[ext]'
      }
    ]
  },
  resolve: {
    alias: {
      images: path.join(__dirname, 'src', 'assets', 'images'),
      fonts: path.join(__dirname, 'src', 'assets', 'fonts'),
      actions: path.join(__dirname, 'src', 'actions'),
      reducers: path.join(__dirname, 'src', 'reducers'),
      helpers: path.join(__dirname, 'src', 'helpers'),
      middleware: path.join(__dirname, 'src', 'middleware'),
      models: path.join(__dirname, 'src', 'models'),
      components: path.join(__dirname, 'src', 'components'),
      containers: path.join(__dirname, 'src', 'containers'),
      constants: path.join(__dirname, 'src', 'constants'),
      node_modules: path.join(__dirname, 'node_modules'),
      utils: path.join(__dirname, 'src', 'utils')
    }
  },
  plugins: [
    new DotenvPlugin({
      sample: './.env.example',
      path: PATHS.env
    }),
    new HtmlWebpackPlugin({ template: 'template.ejs' })
  ]
}

config.development = {
  devtool: 'cheap-module-eval-source-map',
  entry: {
    app: [
      'react-hot-loader/patch',
      PATHS.js,
      PATHS.css
    ]
  },
  output: {
    filename: 'javascripts/[name].js',
  },
  devServer: {
    // host: process.env.HOST,
    port: process.env.PORT,
    hotOnly: true,
    historyApiFallback: true,
    open: true,
    noInfo: true,
    stats: 'errors-only'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.(scss)$/,
        use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']
      }
    ]
  }
}

config.production = {
  devtool: 'nosources-source-map',
  entry: {
    app: [
      PATHS.js,
      PATHS.css
    ]
  },
  output: {
    filename: 'javascripts/[name]-[chunkhash:8].js'
  },
  plugins: [
    new webpack.HashedModuleIdsPlugin(),
    new ExtractTextPlugin('stylesheets/[name]-[contenthash:8].css'),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8
    })
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: ['css-loader', 'postcss-loader', 'sass-loader'],
          fallback: 'style-loader'
        })
      }
    ]
  }
}

config.staging = config.production;

module.exports = merge(config.common, config[process.env.NODE_ENV]);
